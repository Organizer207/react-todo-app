import React from 'react';
import PropTypes from 'prop-types';
import FontAwesome from'react-fontawesome';

function Edit (props){

const buttonStyle={
  float:'right',
}
  const EditKey =()=>{

    props.forEdit()
  }
  return(
      <button className="btn btn-primary" value="Edit" style={buttonStyle} onClick={()=>EditKey() }> <FontAwesome name="pencil" /> </button>
  )
}
Edit.propTypes={
  forEdit: PropTypes.func
}
export default Edit;

import React  from 'react';
import PropTypes from 'prop-types';


function AddTodo(props) {
  let _input = null;
  const Add = (event) => {
    // console.log(event);
    if(_input.value !== ""){
      props.addItem({
        Todo:_input.value,
        isCompleted: false,
        id: (new Date().getTime()),
      });
      _input.value="";
      event.preventDefault();
    } else {
      event.preventDefault()
    }
  };
  return(
      <div className="row">
      <div className="col-2"> </div>
      <div className="col-10 ">
        <form className="form-row">
        <div className=" col-8">
            <input ref={(input) => { _input = input} }  type="text" className="form-control col-12" />
        </div>
        <div className="col-4 col-md-2 ">
            <button type="submit" value="Add" className="btn btn-primary col-8 "  onClick={Add}> Add </button>
        </div>
        </form>
        </div>
      </div>
    );
}

AddTodo.propTypes={
  addItem: PropTypes.func,

  }

export default AddTodo;

import React from 'react';
import Edit from '../Edit'
import './main.css';
import PropTypes from 'prop-types';
import FontAwesome from'react-fontawesome';

function TodoList(props) {
  let editInput = null;
  let checkBox= null;
  const remover =(id)=>{
    props.rmvItem({id:id});
  }


  const EditValue = (id , editInput , todo)=>{
    document.getElementById(todo).style.display="none";
    document.getElementById(id).style.display="inherit";

  }



  const cancle =(id , todo) =>{


    document.getElementById(todo).style.display="inherit";
    document.getElementById(id).style.display="none";
  }


    const changeHandler = (item , input) =>{
      item.Todo=input.target.value;
      props.editeState(item)

    }
    const checkChangeHandler = (item , input) =>{
      if (input.target.checked === true){
      item.isCompleted=true;
      props.compEditeState(item)}
      else{
        item.isCompleted=false;
        props.compEditeState(item)
      }
    }
    const removeButton={
      float:'right',
    }

  const show = () => {
    const items= props.items;

      return (
        items.map((item)=>
        <li key={item.id} className="list-group-item">
          <input className="checkbox" checked={item.isCompleted} type="checkbox" ref={(chk) => {checkBox = chk}} onChange={(e) => checkChangeHandler(item , e)}/>
          <span id={item.Todo}> {item.Todo} </span>
          <div className="Edit form-group-row" id={item.id} >
            <input className="form-control"  defaultValue={item.Todo} onChange={(e) => changeHandler(item , e)} type="text" ref={(a) => {editInput = a}} />
            <button className="btn btn-info" value="Click After Changes Entered" onClick={() => cancle(item.id , item.Todo)}> Click After Changes Entered </button>
          </div>
          <button className="btn btn-danger" value="Remove" style={removeButton} onClick={()=> remover(item.id)}> <FontAwesome name="trash" /> </button>
          <Edit forEdit={() => EditValue(item.id , editInput , item.Todo)} />

        </li> ))

  };

  return(
    <div className="row">
    <div className="col-2"> </div>
    <div className="col-8 border">
      <ul className="list-group list-group-flush">
        {show()}
      </ul>
    </div>
    </div>
  );

}
TodoList.propTypes={
  rmvItem: PropTypes.func,
  editeState:PropTypes.func,
  compEditeState:PropTypes.func,
  items:PropTypes.array,
}
export default TodoList;

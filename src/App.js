import React, { Component } from 'react';
import AddTodo from './components/Add_Todo';
import TodoList from './components/Todo_List';
import FilterKeys from './components/Filter';
import Search from './components/Search';
// import PropTypes from 'prop-types';
import './sass/App.scss';
import 'bootstrap';
import fontawesome from '@fortawesome/fontawesome';
import brands from '@fortawesome/fontawesome-free-brands';
import faCheckSquare from '@fortawesome/fontawesome-free-solid/faCheckSquare';
import faCoffee from '@fortawesome/fontawesome-free-solid/faCoffee';

fontawesome.library.add(brands, faCheckSquare, faCoffee)



class App extends Component {
  constructor(){
    super();
        //Giving Value of Local Storage on Start If Any Exists
    if (localStorage.getItem('Todo_List')) {
    this.state={
      items:  JSON.parse(localStorage.getItem('Todo_List')),
      filters:null,
      status:0
    }
    }else {
      this.state={
        items:[],
        filters:null,
        status:0
      };
    }

  }

//Add New Job
  addItem(newItem) {
    if (localStorage.getItem('Todo_List')) {
    this.setState({
      items:  JSON.parse(localStorage.getItem('Todo_List'))
    });
    }
    this.setState((oldState) => {
      oldState.items.unshift(newItem);
      localStorage.setItem('Todo_List' , JSON.stringify(oldState.items));
      return {items:  oldState.items};
    });

  }

  //Remove a Job From List
  rmvItem(keyValue){
    let array= JSON.parse(localStorage.getItem('Todo_List'))
    let filteredItems = array.filter( (item)=> {
    return (item.id  !== keyValue.id);
  });

  this.setState({
    items: filteredItems
  });
  localStorage.setItem('Todo_List' , JSON.stringify(filteredItems));
}

// Only Compeleted Jobs Will be Visible
completedJobs(){
  if(this.state.status === 0 || this.state.status === -1){
  this.setState({
    status:1
  })
}
else{
  this.setState({
    status:0
})

}
}
// Only InCompleted Jobs Will be Visible
  inCompletedJobs(){
    if(this.state.status === 0 || this.state.status === 1){
    this.setState({
      status:-1
    })
  }
  else{
    this.setState({
      status:0
  })

  }

  }

  EditApp(id , input , todo ){
    console.log('test');
    console.log(input);
  }
  editState(item){
    console.log(item);
    const old = this.state.items.find((t) => t.id === item.id)
    old.Todo = item.Todo;
    this.setState({
      items: this.state.items,
    })
    localStorage.setItem('Todo_List' , JSON.stringify(this.state.items));

    // console.log(this.state.items);
  }
  chkEditState(item){
    console.log(item);
    const old = this.state.items.find((t) => t.id === item.id)
    old.isCompleted = item.isCompleted;
    this.setState({
      items: this.state.items,
    })
    localStorage.setItem('Todo_List' , JSON.stringify(this.state.items));
  }
  submitSearch(value){
    if (value===""){
      this.setState({
        items: JSON.parse(localStorage.getItem('Todo_List')),
        filters : null,
        status: 0

      });
    }
    else{
      this.setState({
        filters: value,
        status: 0
      })

    }
  }


  getFilteredItems() {
    const regex =  RegExp(`.*${this.state.filters}.*`);
    const fields = this.state.items;

    // if(this.state.status === 0){
    //   const fields = this.state.items
    // }

    if(this.state.filters !== null){

      let search = fields.filter( (item)=> {
        return(regex.test(item.Todo));
      });
      return search;
    }
    if(this.state.status === 1){
      let Completed = fields.filter( (item)=> {
        return(item.isCompleted=== true);
      });
      return Completed
    }
    if(this.state.status === -1){
      let inCompleted = fields.filter( (item)=> {
        return(item.isCompleted === false);
      });
      return inCompleted
    }

    else{
      return (this.state.items)
    }
  }

  render() {

    return (
      <div className="container">
        <AddTodo addItem={(newItem) => this.addItem(newItem)} />
          <FilterKeys complete={() => this.completedJobs()} inComplete={() => this.inCompletedJobs()} />
        <TodoList
          items={this.getFilteredItems()}
          rmvItem={(keyValue) => this.rmvItem(keyValue)}
          editApp={(id , input , todo)=> this.EditApp(id , input , todo)}
          editeState={(todo , input) => this.editState(todo , input)}
          compEditeState={(item ) => this.chkEditState(item )}
        />
      <Search searchSubmit={(searchValue) => this.submitSearch(searchValue)} />
      </div>
    );
  }
}


export default App;

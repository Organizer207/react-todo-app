import React from 'react';
import PropTypes from 'prop-types';

function Search (props) {
let search = null;

const searchHandler =(event) =>{
  const Value = event.target.value;
    props.searchSubmit(Value);
}
return (
  <div className="row">
    <div className="col-2"> </div>
    <div className="col-8">
      <input
      className="form-control"
      type="text"
      onChange={(e) => searchHandler( e)}
      placeholder="Enter Value to search"
      ref={(a) => {search = a}}/>
    </div>
  </div>
)

}
Search.propTypes={
searchSubmit:PropTypes.func,

}

export default Search;

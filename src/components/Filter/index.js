import React from 'react';
import PropTypes from 'prop-types';

function FilterKeys (props){

const completed = ()=>{
  props.complete();
}
const inCompleted = ()=>{
  props.inComplete();
}
return(
  <div className="row">
    <div className="col-4"> </div>
    <div className="col-4">
        <button value="Completed" onClick={()=> completed()} className="btn-group-toggle  btn-success col-12 btn-sm col-md-6" data-toggle="buttons"> Completed </button>
      <button value="InCompleted" onClick={()=> inCompleted()}  className="btn-group-toggle  btn-warning col-12 btn-sm col-md-6" data-toggle="buttons"> InCompleted </button>
    </div>
  </div>
)
}
FilterKeys.propTypes={
  complete: PropTypes.func,
  inComplete: PropTypes.func,
}
export default FilterKeys;
